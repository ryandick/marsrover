#Mars Rover

## Dependencies

### Nodejs
```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
source ~/.bashrc
nvm install v10
```

### Modules
```
npm install
```

## Test
```
npm run test
```