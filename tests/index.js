const Rover = require('../app/lib/');
const expect = require('expect.js');

describe('rover 1', function () {
    it('initialises at determined position without direction', function () {
        let rover = new Rover(5, 5);
        expect(rover.maxX).to.be(5);
        expect(rover.maxY).to.be(5);
        expect(rover.direction).to.be('N');
    });
    it('initialises at determined position and direction', function () {
        let rover = new Rover(1, 2);
        expect(rover.maxX).to.be(1);
        expect(rover.maxY).to.be(2);
        expect(rover.direction).to.be('N');
    });
    it('move rover by LMLMLMLMM on 5x5 grid deployed to 1,2,N', function () {
        let rover = new Rover(5, 5);
        expect(rover.maxX).to.be(5);
        expect(rover.maxY).to.be(5);
        rover.deploy(1,2,'N');
        expect(rover.x).to.be(1);
        expect(rover.y).to.be(2);
        expect(rover.direction).to.be('N');
        rover.action('LMLMLMLMM');
        expect(rover.y).to.be(2);
        expect(rover.x).to.be(3);
        expect(rover.direction).to.be('N');
    });
    it('move rover by MMRMMRMRRM on 5x5 grid deployed to 3,3,E', function () {
        let rover = new Rover(5, 5);
        expect(rover.maxX).to.be(5);
        expect(rover.maxY).to.be(5);
        rover.deploy(3,3,'E');
        expect(rover.x).to.be(3);
        expect(rover.y).to.be(3);
        expect(rover.direction).to.be('E');
        rover.action('MMRMMRMRRM');
        expect(rover.y).to.be(5);
        expect(rover.x).to.be(1);
        expect(rover.direction).to.be('E');
    });
});