/**
 * main rover class, define max grid size for exploration
 * @param {Number} maxX 
 * @param {Number} maxY
 * @class 
 */
let Rover = function (maxX, maxY) {
    this.x = 0;
    this.y = 0;
    this.maxX = maxX;
    this.maxY = maxY;
    this.direction = 'N';
};

/**
 * deploy rover to grid with specified orientation
 * @param {Number} x 
 * @param {Number} y 
 * @param {String} direction [N,S,E,W]
 * @public 
 */
Rover.prototype.deploy = function(x, y, direction){
    this.x = x;
    this.y = y;
    this.direction = direction || 'N';
}

/**
 * pass instructions to rover for movement and orientation, can be L,R or M
 * @param {String} instructions [L,R,M]
 * @public
 */
Rover.prototype.action = function (instructions) {
    instructions = instructions.split('');
    instructions.forEach(instruction => {
        switch (instruction) {
            case 'M':
                this.move();
                break;
            case 'L':
                this.turnLeft();
                break;
            case 'R':
                this.turnRight();
                break;
            default:
                break;
        }
    });
};

/**
 * turn rover left
 * @private
 */
Rover.prototype.turnLeft = function () {
    switch (this.direction) {
        case 'N':
            this.direction = 'W'
            break;
        case 'W':
            this.direction = 'S'
            break;
        case 'S':
            this.direction = 'E';
            break;
        case 'E':
            this.direction = 'N';
            break;
        default:
            throw new Error('rover direction is not valid');
            break;
    }
};
/**
 * turn rover right
 * @private
 */
Rover.prototype.turnRight = function () {
    switch (this.direction) {
        case 'N':
            this.direction = 'E'
            break;
        case 'E':
            this.direction = 'S'
            break;
        case 'S':
            this.direction = 'W';
            break;
        case 'W':
            this.direction = 'N';
            break;
        default:
            throw new Error('rover direction is not valid');
            break;
    }
};
/**
 * move rover one square on grid according to orientation
 * @private
 */
Rover.prototype.move = function () {
    switch (this.direction) {
        case 'N':
            if (this.x + 1 <= this.maxX) this.x += 1;
            break;
        case 'E':
            if (this.y + 1 <= this.maxY) this.y += 1;
            break;
        case 'S':
            if (this.x - 1 > 0) this.x -= 1;
            break;
        case 'W':
            if (this.y - 1 > 0) this.y -= 1;
            break;
        default:
            throw new Error('rover direction is not valid');
            break;
    }
};

module.exports = Rover;